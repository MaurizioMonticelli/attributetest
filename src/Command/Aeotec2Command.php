<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:28
 */

namespace App\Command;


use App\Model\Attributes\Aeotec\AeoTecMultiSensor6Attribute;
use App\Model\Attributes\HumidityAttribute;
use App\Model\Attributes\Interfaces\TemperatureInterface;
use App\Model\Attributes\LightBrightnessAttribute;
use App\Model\Attributes\NumberAttribute;
use App\Model\Attributes\TemperatureAttribute;
use App\Model\Attributes\TemperatureIndoorAttribute;
use App\Model\Attributes\TemperatureOutdoorAttribute;
use App\Model\Device\Aeotec\AeoTecMultiSensor6;
use App\Model\Device\AttributeListDevice;
use App\Model\Device\DeviceAttribute;
use App\Model\Device\DeviceAttribute\TemparetureOutdoorDeviceAttribute;
use App\Model\Device\HueDevice;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Aeotec2Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName($this->getCommandName());
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $importData = [
            'name' => 'AeoTec 1243',
            'attributes' => [
                'lux' => 'Numeric', 'hum' => 'Numeric', 'uv'  => 'Numeric', 'pir'  => 'Binary', 'temp'  => 'Numeric', 'batt'  => 'Numeric',
            ]
        ];


        $aeoTecMultiSensor6 = new AttributeListDevice();

        $aeoTecMultiSensor6->setName('AeoTech Multisensor Zimmer');


        $att = new DeviceAttribute();
        $att->setAttribute(new HumidityAttribute($att));
        $att->setKey('hum');
        $aeoTecMultiSensor6->getDeviceAttributes()->add($att);

        $att = new DeviceAttribute();
        $att->setAttribute(new TemperatureAttribute($att));
        $att->setKey('temp');
        $aeoTecMultiSensor6->getDeviceAttributes()->add($att);

        $att = new DeviceAttribute();
        $att->setAttribute(new TemperatureOutdoorAttribute($att));
        $att->setKey('temp_out');
        $aeoTecMultiSensor6->getDeviceAttributes()->add($att);


        $att = new DeviceAttribute();
        $nestedAttr = new TemperatureIndoorAttribute($att);
        $nestedAttr->setMaxValue(60);
        $nestedAttr->setValue(55);
        $att->setAttribute($nestedAttr);
        $att->setKey('temp_in');
//        $att->setMaxValue(50);
        $aeoTecMultiSensor6->getDeviceAttributes()->add($att);


        foreach ($aeoTecMultiSensor6->getDeviceAttributes() as $deviceAttribute) {
            $output->writeln('key is: ' . $deviceAttribute->getKey());
            $output->writeln('ATTKEY is: ' . $deviceAttribute->getParam());
            $output->writeln('value is: ' . $deviceAttribute->getValue());
            if ( $deviceAttribute->getAttribute() instanceof TemperatureIndoorAttribute) {
                $output->writeln('maxValue is: ' . $deviceAttribute->getAttribute()->getMaxValue());
            }

            if ( in_array(TemperatureInterface::class, class_implements($deviceAttribute->getAttribute()))) {
                $output->writeln('implements  TemperatureInterface' );
            }

        }


        $output->writeln('-----');

        $aeoTecMultiSensor6->getDeviceAttributes()->setValue(TemperatureIndoorAttribute::class, 30);
//        $aeoTecMultiSensor6->getDeviceAttributes()->setValue('hum', 30);
        $aeoTecMultiSensor6->getDeviceAttributes()->setValue('temp', 'hot');
        $aeoTecMultiSensor6->getDeviceAttributes()->setValue('temp_in', 'cold');
        $aeoTecMultiSensor6->getDeviceAttributes()->setValue('temp_out', 11);




        foreach ($aeoTecMultiSensor6->getDeviceAttributes() as $deviceAttribute) {
            $output->writeln('key is: ' . $deviceAttribute->getKey());
            $output->writeln('ATTKEY is: ' . $deviceAttribute->getParam());
            $output->writeln('value is: ' . $deviceAttribute->getValue());
        }

//        $aeoTecMultiSensor6->setDeviceAttribute(new AeoTecMultiSensor6Attribute());
//
//        $aeoTecMultiSensor6->getDeviceAttribute()->setHumidityValue(50);
//        $aeoTecMultiSensor6->getDeviceAttribute()->setLiminecenceValue(50);
//        $aeoTecMultiSensor6->getDeviceAttribute()->setTempValue(50);
//        $aeoTecMultiSensor6->getDeviceAttribute()->setUvValue(50);
//        $aeoTecMultiSensor6->getDeviceAttribute()->setPirValue(50);
//
//
//        $output->writeln('<comment>Humidity is: ' . $aeoTecMultiSensor6->getDeviceAttribute()->getHumidityValue() . '</comment>');
//        $output->writeln('<comment>Liminecence is: ' . $aeoTecMultiSensor6->getDeviceAttribute()->getLiminecenceValue() . '</comment>');
//        $output->writeln('<comment>Temp is: ' . $aeoTecMultiSensor6->getDeviceAttribute()->getTempValue() . '</comment>');
//        $output->writeln('<comment>Uv is: ' . $aeoTecMultiSensor6->getDeviceAttribute()->getUvValue() . '</comment>');
//        $output->writeln('<comment>PIR is: ' . ($aeoTecMultiSensor6->getDeviceAttribute()->getPirValue() ? 'on' : 'off') . '</comment>');




    }

    /**
     * @return string
     */
    protected function getCommandName(): string
    {
        return 'test:aeotec2';
    }

}