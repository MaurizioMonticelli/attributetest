<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:28
 */

namespace App\Command;


use App\Model\Attributes\LightBrightnessAttribute;
use App\Model\Device\HueDevice;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('test:hue');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $myHueDevice = new HueDevice();

        $myHueDevice->setName('Hue Lampe 1');


        $myHueDevice->setDeviceAttribute(new LightBrightnessAttribute());

        $myHueDevice->getDeviceAttribute()->setBrightnessValue(50);

        $output->writeln('<comment>Bightness is: ' . $myHueDevice->getDeviceAttribute()->getBrightnessValue() . '</comment>');
        $output->writeln('<comment>Light is: ' . ($myHueDevice->getDeviceAttribute()->getBinaryValue() ? 'on' : 'off') . '</comment>');


        $myHueDevice->getDeviceAttribute()->setBinaryValue(false);

        $output->writeln('<comment>Bightness is: ' . $myHueDevice->getDeviceAttribute()->getBrightnessValue() . '</comment>');
        $output->writeln('<comment>Light is: ' . ($myHueDevice->getDeviceAttribute()->getBinaryValue() ? 'on' : 'off') . '</comment>');


        $myHueDevice->getDeviceAttribute()->setBinaryValue(true);

        $output->writeln('<comment>Bightness is: ' . $myHueDevice->getDeviceAttribute()->getBrightnessValue() . '</comment>');
        $output->writeln('<comment>Light is: ' . ($myHueDevice->getDeviceAttribute()->getBinaryValue() ? 'on' : 'off') . '</comment>');



    }

}