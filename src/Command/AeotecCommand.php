<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:28
 */

namespace App\Command;


use App\Model\Attributes\Aeotec\AeoTecMultiSensor6Attribute;
use App\Model\Attributes\LightBrightnessAttribute;
use App\Model\Device\Aeotec\AeoTecMultiSensor6;
use App\Model\Device\HueDevice;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AeotecCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('test:aeotec');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $importData = [
            'name' => 'AeoTec 1243',
            'attributes' => [
                'lux' => 'Numeric', 'uv'  => 'Numeric', 'pir'  => 'Binary', 'temp'  => 'Numeric', 'batt'  => 'Numeric',
            ]
        ];


        $myHueDevice = new AeoTecMultiSensor6();

        $myHueDevice->setName('AeoTech Multisensor Zimmer');


        $myHueDevice->setDeviceAttribute(new AeoTecMultiSensor6Attribute());

        $myHueDevice->getDeviceAttribute()->setHumidityValue(50);
        $myHueDevice->getDeviceAttribute()->setLiminecenceValue(50);
        $myHueDevice->getDeviceAttribute()->setTempValue(50);
        $myHueDevice->getDeviceAttribute()->setUvValue(50);
        $myHueDevice->getDeviceAttribute()->setPirValue(50);


        $output->writeln('<comment>Humidity is: ' . $myHueDevice->getDeviceAttribute()->getHumidityValue() . '</comment>');
        $output->writeln('<comment>Liminecence is: ' . $myHueDevice->getDeviceAttribute()->getLiminecenceValue() . '</comment>');
        $output->writeln('<comment>Temp is: ' . $myHueDevice->getDeviceAttribute()->getTempValue() . '</comment>');
        $output->writeln('<comment>Uv is: ' . $myHueDevice->getDeviceAttribute()->getUvValue() . '</comment>');
        $output->writeln('<comment>PIR is: ' . ($myHueDevice->getDeviceAttribute()->getPirValue() ? 'on' : 'off') . '</comment>');




    }

}