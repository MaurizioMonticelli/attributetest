<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:30
 */

namespace App\Model\Device;


use App\Model\Attributes\AttributeBase;

class AttributeListDevice
{

    public $id;
    public $name;


    /** @var DeviceAttributeList */
    public $deviceAttributes;


    public function __construct()
    {
        $this->deviceAttributes = new DeviceAttributeList();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return DeviceAttributeList
     */
    public function getDeviceAttributes(): DeviceAttributeList
    {
        return $this->deviceAttributes;
    }







}