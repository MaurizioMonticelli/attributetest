<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 14:51
 */

namespace App\Model\Device\Aeotec;


use App\Model\Attributes\Aeotec\AeoTecMultiSensor6Attribute;
use App\Model\Attributes\Interfaces\Devices\Aeotec\AeoTecMultiSensor6Interface;
use App\Model\Device\Device;

class AeoTecMultiSensor6 extends Device implements AeoTecMultiSensor6Interface
{

    public function getDeviceAttribute(): AeoTecMultiSensor6Attribute
    {
        return $this->deviceAttribute;
    }

    public function setDeviceAttribute(AeoTecMultiSensor6Attribute $att): void
    {
        $this->deviceAttribute = $att;
    }
}