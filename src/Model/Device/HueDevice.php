<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:30
 */

namespace App\Model\Device;


use App\Model\Attributes\Interfaces\Devices\DimmableLightInterface;
use App\Model\Attributes\LightBrightnessAttribute;

class HueDevice extends Device implements DimmableLightInterface
{

    public function __construct()
    {
       parent::__construct();


    }


    public function getDeviceAttribute(): LightBrightnessAttribute
    {
        return $this->deviceAttribute;
    }

    public function setDeviceAttribute(LightBrightnessAttribute $att): void
    {
        $this->deviceAttribute = $att;
    }
}