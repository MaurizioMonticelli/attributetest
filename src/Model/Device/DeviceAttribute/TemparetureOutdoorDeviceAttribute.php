<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 16:12
 */

namespace App\Model\Device\DeviceAttribute;


use App\Model\Attributes\AttributeBase;
use App\Model\Attributes\AttributeBaseInterface;
use App\Model\Attributes\TemperatureIndoorAttribute;
use App\Model\Device\DeviceAttribute;

class TemparetureOutdoorDeviceAttribute extends DeviceAttribute
{

    protected $maxValue = null;


    public function __construct()
    {
        $this->setAttribute(new TemperatureIndoorAttribute());
    }

    /**
     * @return mixed
     */
    public function getMaxValue()
    {
        if ($this->maxValue == null) {
            return $this->getAttribute()->getMaxValue();
        }
        return $this->maxValue;
    }

    /**
     * @param mixed $maxValue
     */
    public function setMaxValue($maxValue): void
    {
        $this->maxValue = $maxValue;
    }





}