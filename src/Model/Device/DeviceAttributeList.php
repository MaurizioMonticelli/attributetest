<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 15:18
 */

namespace App\Model\Device;


use App\Model\Attributes\AttributeBaseInterface;
use App\Model\Attributes\NumberAttribute;

class DeviceAttributeList implements  \Iterator , \SeekableIterator , \Countable , \Serializable
{

    /** @var array DeviceAttribute[] */
    private $list = [];
    private $position = null;

    public function add(DeviceAttribute $deviceAttribute)
    {

        $this->list[$deviceAttribute->getKey()] = $deviceAttribute;

    }

    public function setValue($key, $value)
    {
        foreach ($this->list as $item) {
            if ($item->getKey() == $key) {
                    $item->setValue($value);
            }
        }

    }


    /**
     * @return DeviceAttribute
     */
    public function current():DeviceAttribute
    {
        return $this->list[$this->position];
    }

    /**
     * Updates $this->position pointer.
     */
    public function next()
    {
        $keys = array_keys($this->list);
        $position = array_search($this->position, $keys);
        if (isset($keys[$position + 1])) {
            $this->position = $keys[$position + 1];
        } else {
            ++$this->position;
        }
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if all $this->list are valid objects.
     *
     * @return bool
     */
    public function valid()
    {
        return
            isset($this->list[$this->position]) &&
            $this->list[$this->position] instanceof DeviceAttribute;
    }

    /**
     * Resets $this->position pointer.
     */
    public function rewind()
    {
        $this->position = array_keys($this->list)[0];
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->list);
    }

    /**
     * @return DeviceAttribute
     */
    public function last()
    {
        return array_slice($this->list, -1, 1)[0];
    }

    public function serialize()
    {
        return serialize(array(
            $this->list,
            $this->position,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->list,
            $this->position,
            ) = unserialize($serialized);
    }

    /**
     * Seeks to a position
     * @link http://php.net/manual/en/seekableiterator.seek.php
     * @param int $position <p>
     * The position to seek to.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function seek($position)
    {
        $this->position = $position;
    }
}