<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 16:12
 */

namespace App\Model\Device;


use App\Model\Attributes\AttributeBase;
use App\Model\Attributes\AttributeBaseInterface;

class DeviceAttribute
{

    protected $klass;
    protected $key;
    protected $value;
    protected $attributeParam = [];


    /**
     * @return mixed
     */
    public function getAttribute() : AttributeBase
    {
        return new $this->klass($this);
    }

    /**
     * @param mixed $klass
     */
    public function setAttribute(AttributeBase $attribute): void
    {
        $this->klass = get_class($attribute);;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @param mixed $value
     */
    public function getParam(): string
    {
        $obj = new \ReflectionClass(  $this->klass );
        return $obj->getconstant( "ATTKEY" );
    }

    public function setAttributePram($key, $value)
    {
        $this->attributeParam[$key] = $value;
    }

    public function getAttributePram($key)
    {
        return $this->attributeParam[$key];
    }

    public function hasAttributePram($key)
    {
        return isset($this->attributeParam[$key]);
    }

}