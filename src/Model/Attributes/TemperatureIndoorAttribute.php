<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes;


use App\Model\Attributes\Interfaces\TemperatureInterface;

class TemperatureIndoorAttribute extends NumberAttribute implements TemperatureInterface
{
    const ATTKEY = 'teperature_indoor';
    const MAX_VALUE = 'maxValue';

    protected $maxValue = 30;
    protected $minValue = 10;

    /**
     * @return mixed
     */
    public function getMaxValue() : int
    {
        if ($this->deviceAttribute->hasAttributePram(self::MAX_VALUE)) {
            return $this->deviceAttribute->getAttributePram(self::MAX_VALUE);
        }

        return $this->maxValue;
    }

    /**
     * @param mixed $maxValue
     */
    public function setMaxValue($maxValue): void
    {
        $this->maxValue = $maxValue;
        $this->deviceAttribute->setAttributePram(self::MAX_VALUE, $maxValue);
    }

    /**
     * @return mixed
     */
    public function getMinValue()
    {
        return $this->minValue;
    }

    /**
     * @param mixed $minValue
     */
    public function setMinValue($minValue): void
    {
        $this->minValue = $minValue;
    }



}