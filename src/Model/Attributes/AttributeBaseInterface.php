<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes;


interface AttributeBaseInterface
{

    /**
     * @return mixed
     */
    public function getName(): string;
    /**
     * @param mixed $name
     */
    public function setName($name): void;



}