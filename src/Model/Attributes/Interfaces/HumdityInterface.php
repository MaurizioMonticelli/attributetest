<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface HumdityInterface
{
    public function getHumidityValue() : float;
    public function setHumidityValue(float $val) : void;

}