<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface BrightnessInterface
{
    public function getBrightnessValue() : float;
    public function setBrightnessValue(float $val) : void;
}