<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface NumberInterface
{
    public function getNumericValue() : float;
    public function setNumericValue(float $val) : void;
}