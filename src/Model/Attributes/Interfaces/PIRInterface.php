<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface PIRInterface
{
    public function getPirValue() : bool;
    public function setPirValue(bool $val) : void;
}