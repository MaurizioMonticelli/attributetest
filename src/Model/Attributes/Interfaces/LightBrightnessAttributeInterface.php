<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 14:05
 */

namespace App\Model\Attributes\Interfaces;


interface LightBrightnessAttributeInterface extends BinaryInterface, NumberInterface
{

}