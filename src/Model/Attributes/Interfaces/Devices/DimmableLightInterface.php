<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 14:07
 */

namespace App\Model\Attributes\Interfaces\Devices;


use App\Model\Attributes\Interfaces\LightBrightnessAttributeInterface;
use App\Model\Attributes\LightBrightnessAttribute;

interface DimmableLightInterface
{

    public function getDeviceAttribute() : LightBrightnessAttribute;
    public function setDeviceAttribute(LightBrightnessAttribute $att) : void ;
}