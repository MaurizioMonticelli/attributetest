<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 14:49
 */

namespace App\Model\Attributes\Interfaces\Devices\Aeotec;


use App\Model\Attributes\Aeotec\AeoTecMultiSensor6Attribute;

interface AeoTecMultiSensor6Interface
{
    public function getDeviceAttribute() : AeoTecMultiSensor6Attribute;
    public function setDeviceAttribute(AeoTecMultiSensor6Attribute $att) : void ;

}