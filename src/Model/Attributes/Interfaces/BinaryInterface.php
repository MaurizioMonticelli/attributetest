<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface BinaryInterface
{
    public function getBinaryValue() : bool;
    public function setBinaryValue(bool $val) : void;
}