<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface LuminecenceInterface
{
    public function getLiminecenceValue() : int;
    public function setLiminecenceValue(int $val) : void;
}