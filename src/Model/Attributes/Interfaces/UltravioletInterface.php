<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 13:54
 */

namespace App\Model\Attributes\Interfaces;


interface UltravioletInterface
{
    public function getUvValue() : float;
    public function setUvValue(float $val) : void;
}