<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes;


use App\Model\Device\DeviceAttribute;

class AttributeBase implements AttributeBaseInterface
{

    const ATTKEY = 'none';
    public $name;
    protected $deviceAttribute;

    public function __construct(DeviceAttribute $deviceAttribute)
    {
        $this->deviceAttribute = $deviceAttribute;
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    static function getAttributeKey()  {
        return static::ATTKEY;
    }


    public function getValue() {
        $this->deviceAttribute->getValue();
    }

    public function setValue($val) {
        $this->deviceAttribute->setValue($val);
    }

}

