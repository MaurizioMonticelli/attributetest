<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes\Aeotec;


use App\Model\Attributes\AttributeBase;
use App\Model\Attributes\Interfaces\BinaryInterface;
use App\Model\Attributes\Interfaces\BrightnessInterface;
use App\Model\Attributes\Interfaces\HumdityInterface;
use App\Model\Attributes\Interfaces\LuminecenceInterface;
use App\Model\Attributes\Interfaces\NumberInterface;
use App\Model\Attributes\Interfaces\PIRInterface;
use App\Model\Attributes\Interfaces\TemperatureInterface;
use App\Model\Attributes\Interfaces\UltravioletInterface;

class AeoTecMultiSensor6Attribute  extends AttributeBase implements HumdityInterface, LuminecenceInterface, TemperatureInterface, PIRInterface, UltravioletInterface
{

    const TEMPERATURE = 'temperature';
    const HUMIDITY = 'humidity';
    const ULTRAVIOLET = 'ultraviolet';
    const LUMINESCENCE = 'LUMINESCENCE';
    const PIR = 'pir';


    public function __construct()
    {
        $this->params[self::TEMPERATURE] = 0;
        $this->params[self::HUMIDITY] = 0;
        $this->params[self::ULTRAVIOLET] = 0;
        $this->params[self::LUMINESCENCE] = 0;
        $this->params[self::PIR] = false;
    }

    public function getHumidityValue(): float
    {
        return $this->params[self::HUMIDITY];
    }

    public function setHumidityValue(float $val): void
    {
        $this->params[self::HUMIDITY] = $val;
    }

    public function getLiminecenceValue(): int
    {
        return $this->params[self::LUMINESCENCE];
    }

    public function setLiminecenceValue(int $val): void
    {
        $this->params[self::LUMINESCENCE] = $val;
    }

    public function getTempValue(): float
    {
        return $this->params[self::TEMPERATURE];
    }

    public function setTempValue(float $val): void
    {
        $this->params[self::TEMPERATURE] = $val;
    }

    public function getUvValue(): float
    {
        return $this->params[self::ULTRAVIOLET];
    }

    public function setUvValue(float $val): void
    {
        $this->params[self::ULTRAVIOLET] = $val;
    }

    public function getPirValue(): bool
    {
        return $this->params[self::PIR];
    }

    public function setPirValue(bool $val): void
    {
        $this->params[self::PIR] = $val;
    }
}