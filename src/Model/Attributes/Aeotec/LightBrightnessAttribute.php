<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes\Aeotec;


use App\Model\Attributes\Interfaces\BinaryInterface;
use App\Model\Attributes\Interfaces\BrightnessInterface;
use App\Model\Attributes\Interfaces\HumdityInterface;
use App\Model\Attributes\Interfaces\LuminecenceInterface;
use App\Model\Attributes\Interfaces\NumberInterface;
use App\Model\Attributes\Interfaces\PIRInterface;
use App\Model\Attributes\Interfaces\TemperatureInterface;
use App\Model\Attributes\Interfaces\UltravioletInterface;

class LightBrightnessAttribute extends AttributeBase implements BinaryInterface, BrightnessInterface
{

    const BRIGHTNESS = 'BRIGHTNESS';


    public function __construct()
    {
        $this->params[self::BRIGHTNESS] = 0;
    }


    public function getBinaryValue(): bool
    {
        return $this->params[self::BRIGHTNESS] > 0;
    }

    public function getBrightnessValue(): float
    {
        return $this->params[self::BRIGHTNESS];
    }


    public function setBinaryValue(bool $val): void
    {
        $this->params[self::BRIGHTNESS] = $val ? 100 : 0;
    }

    public function setBrightnessValue(float $val): void
    {
        $this->params[self::BRIGHTNESS] = $val;
    }


}