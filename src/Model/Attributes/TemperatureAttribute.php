<?php
/**
 * Created by PhpStorm.
 * User: mauri
 * Date: 14.06.18
 * Time: 11:31
 */

namespace App\Model\Attributes;


use App\Model\Attributes\Interfaces\TemperatureInterface;

class TemperatureAttribute extends NumberAttribute implements TemperatureInterface
{
    const ATTKEY = 'teperature';
}